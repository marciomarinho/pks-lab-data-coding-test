# PKS coding test
Patient data from laboratory are exported into CSV file. One line in CSV file represents one attribute with four fields - date, patientId, attribute name and attribute value. 

Attribute and their values are grouped into episodes. An episode is a group of all attributes for one patient on a single day.

```
Date, PatientId, Attribute Name, Attribute Value
2019-12-09, 1, Age, 23
2019-12-09, 1, Blood pressure, 160
2019-12-09, 1, Gender, F
2019-12-09, 1, Glucose, 11.1
2019-12-09, 1, Diabetes, TRUE
2019-12-09, 1, WCC, 120
2019-12-09, 2, Age, 24
2019-12-09, 2, Blood pressure, 190
2019-12-09, 2, Glucose, 5.5
2019-12-09, 2, Diabetes, FALSE
2019-12-09, 2, Gender, M
2019-12-09, 2, WCC, 312
2019-12-11, 1, Age, 23
2019-12-11, 1, Blood pressure, 180
2019-12-11, 1, Glucose, 12.2
2019-12-11, 1, Diabetes, TRUE
2019-12-11, 1, WCC, 212
2019-12-14, 3, Age, 26
2019-12-14, 3, Blood pressure, 210
2019-12-14, 3, Glucose, 9.7
2019-12-14, 3, Diabetes, TRUE
2019-12-14, 3, WCC, 110
2019-12-14, 3, Gender, F
```

Your task is to provide an implementation to:

* read data from the CSV file.
* print number of female patients.
* print number of male patients.
* print average age of male patients.
* print average age of female patients.

When considering your design, bear in mind that that your product manager plans to add more functionality to the system like:

* finding all patients with increasing Glucose levels,
* printing a report of average blood pressure for patients in a specified age range,
* etc
 
# Solution

Parsing laboratory data from CSV and returning patients statistics.

## About the project

The project is a Spring Boot application, and I'm reading the sample file from the resources folder.

Technologies used:

* Java 8
* Spring Boot
* Gradle
* JUnit
* Jacoco ( tests coverage )

## Installation

```bash
git clone https://marciomarinho@bitbucket.org/marciomarinho/pks-lab-data-coding-test.git
cd pks-lab-data-coding-test
./gradlew build
java -jar build/libs/pks-lab-data-coding-test-0.0.1-SNAPSHOT.jar
```
Or you can also import the project into your preferred IDE and run from there.

## Usage

After the application is running, a http service will be available to return the file statistics. 
You can make the following call from the command line or use any other tool to send the request.

Request standard statistics from the sample file ( genders count and average age by gender ):

```
curl http://localhost:8080/patient/stats/standard
```

Response:

```
{
  "countByGender": {
    "F": 2,
    "M": 1
  },
  "averageAgeByGender": {
    "F": 24.5,
    "M": 24
  }
}
``` 

Return statistics from patients with increasing glucose levels. This is a mock only, as per requirements
this function will be asked by the product owner in the future, and the methods are there with no calculation to show
the design's flexibility :

```
curl http://localhost:8080/patient/stats/glucose
```

Response:

```
{
  "patients": [
    -1,
    -2,
    -3
  ]
}
```

Return statistics from average blood pressure for patients in a specified age range. 
This is a mock only, as per requirements this function will be asked by the product owner in the future, 
and the methods are there with no calculation to show the design's flexibility :

```
curl  "http://localhost:8080/patient/stats/blood-pressure?startAge=25&endAge=45"
```

Response:

```
{
  "startAge": 25,
  "endAge": 45,
  "averageBloodPressure": 75.8
}
```

## Tests

The project is covered by both Unit and Integration tests.

Jacoco has been used for code coverage, which you can find in the following folder after 
running a build : {rootFolder}/build/reports/jacoco/test/html

Jacoco screenshot1:
![alt text](jacoco1.png)

Jacoco screenshot2:
![alt text](jacoco2.png)

Jacoco screenshot3:
![alt text](jacoco3.png)

Jacoco screenshot4:
![alt text](jacoco4.png)

## Contributing
I would love to have a discussion about the project!

## License
[MIT](https://choosealicense.com/licenses/mit/)