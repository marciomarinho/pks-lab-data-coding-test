package au.com.pks.challenge.service;

import au.com.pks.challenge.domain.LabDataType;
import au.com.pks.challenge.domain.LabPatientDataRow;
import au.com.pks.challenge.domain.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@SpringBootTest
class LogFileReaderServiceTest {

    @Mock
    private AttributeDataSetterService attributeDataSetterService;

    private LogFileReaderService logFileReaderService;

    @BeforeEach
    public void setup() {
        logFileReaderService = new LogFileReaderService(attributeDataSetterService);
    }

    @Test
    public void shouldReadSampleFile() {

        List<Patient> patients = logFileReaderService.readLaboratoryData(LabDataType.SAMPLE);
        assertThat(patients.size(), is(3));
        verify(attributeDataSetterService, times(23))
                .setDataAttribute(ArgumentMatchers.any(LabPatientDataRow.class), ArgumentMatchers.any(Patient.class));

    }

    @Test
    public void shouldReturnEmptyListWhenCatchingException() {

        List<Patient> patients = logFileReaderService.readLaboratoryData(LabDataType.BAD_FORMATTED_FILE);
        assertThat(patients, is(empty()));
        verifyNoInteractions(attributeDataSetterService);

    }
}