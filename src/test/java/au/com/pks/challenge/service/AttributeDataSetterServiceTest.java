package au.com.pks.challenge.service;

import au.com.pks.challenge.domain.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AttributeDataSetterServiceTest {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final LabPatientDataRow age = LabPatientDataRow.builder()
            .date(LocalDate.parse("2019-12-09", formatter))
            .patientId(2)
            .attributeName("Age")
            .attributeValue("24")
            .build();

    private final LabPatientDataRow bloodPressure = LabPatientDataRow.builder()
            .date(LocalDate.parse("2019-12-09", formatter))
            .patientId(2)
            .attributeName("Blood pressure")
            .attributeValue("190")
            .build();

    private final LabPatientDataRow glucose = LabPatientDataRow.builder()
            .date(LocalDate.parse("2019-12-09", formatter))
            .patientId(2)
            .attributeName("Glucose")
            .attributeValue("5.5")
            .build();

    private final LabPatientDataRow diabetes = LabPatientDataRow.builder()
            .date(LocalDate.parse("2019-12-09", formatter))
            .patientId(2)
            .attributeName("Diabetes")
            .attributeValue("FALSE")
            .build();

    private final LabPatientDataRow gender = LabPatientDataRow.builder()
            .date(LocalDate.parse("2019-12-09", formatter))
            .patientId(2)
            .attributeName("Gender")
            .attributeValue("M")
            .build();

    private final LabPatientDataRow wcc = LabPatientDataRow.builder()
            .date(LocalDate.parse("2019-12-09", formatter))
            .patientId(2)
            .attributeName("WCC")
            .attributeValue("312")
            .build();

    private final LabPatientDataRow unknown = LabPatientDataRow.builder()
            .date(LocalDate.parse("2019-12-09", formatter))
            .patientId(2)
            .attributeName("BLAHBLAH")
            .attributeValue("312")
            .build();

    private Patient patient;

    private AttributeDataSetterService attributeDataSetterService;

    @BeforeEach
    public void setup() {
        patient = Patient.builder()
                .examAttribute(new ArrayList<>())
                .build();
        attributeDataSetterService = new AttributeDataSetterService();
    }

    @Test
    public void shouldSetAgeToPatient() {
        attributeDataSetterService.setDataAttribute(age, patient);
        assertThat(patient.getAge(), is(24));
    }

    @Test
    public void shouldSetGenderToPatient() {
        attributeDataSetterService.setDataAttribute(gender, patient);
        assertThat(patient.getGender(), is(gender.getAttributeValue()));
    }

    @Test
    public void shouldSetBloodPressureToPatient() {
        attributeDataSetterService.setDataAttribute(bloodPressure, patient);
        assertThat(patient.getExamAttribute().get(0), is(new BloodPressureAttribute(Integer.parseInt(bloodPressure.getAttributeValue()),
                bloodPressure.getDate())));
    }

    @Test
    public void shouldSetGlucoseToPatient() {
        attributeDataSetterService.setDataAttribute(glucose, patient);
        assertThat(patient.getExamAttribute().get(0), is(new GlucoseAttribute(Float.parseFloat(glucose.getAttributeValue()),
                glucose.getDate())));
    }

    @Test
    public void shouldSetDiabetesToPatient() {
        attributeDataSetterService.setDataAttribute(diabetes, patient);
        assertThat(patient.getExamAttribute().get(0), is(new DiabetesAttribute(Boolean.parseBoolean(diabetes.getAttributeValue()),
                diabetes.getDate())));
    }

    @Test
    public void shouldSetWccToPatient() {
        attributeDataSetterService.setDataAttribute(wcc, patient);
        assertThat(patient.getExamAttribute().get(0), is(new WccAttribute(Integer.parseInt(wcc.getAttributeValue()),
                wcc.getDate())));
    }

    @Test
    public void shouldThrowExceptionWhenAttributeIsNotIdentified() {
        assertThrows(IllegalArgumentException.class, () -> {
            attributeDataSetterService.setDataAttribute(unknown, patient);
        });
    }

}