package au.com.pks.challenge.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class PatientStatsControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnStandardStatisticsForAllPatients() throws Exception {

        this.mockMvc.perform(get("/patient/stats/standard"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.countByGender['F']", is(2)))
                .andExpect(jsonPath("$.countByGender['M']", is(1)))
                .andExpect(jsonPath("$.averageAgeByGender['F']", is(24.5)))
                .andExpect(jsonPath("$.averageAgeByGender['M']", is(24.0)));

    }

    @Test
    public void shouldReturnAllPatientsWithIncreasingGlucoseLevels() throws Exception {

        this.mockMvc.perform(get("/patient/stats/glucose"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.patients[0]", is(-1)))
                .andExpect(jsonPath("$.patients[1]", is(-2)))
                .andExpect(jsonPath("$.patients[2]", is(-3)));

    }

    @Test
    public void shouldReturnAverageBloodPressureForAgeRange() throws Exception {

        this.mockMvc.perform(get("/patient/stats/blood-pressure?startAge=25&endAge=45"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.startAge", is(25)))
                .andExpect(jsonPath("$.endAge", is(45)))
                .andExpect(jsonPath("$.averageBloodPressure", is(75.8)));

    }

}