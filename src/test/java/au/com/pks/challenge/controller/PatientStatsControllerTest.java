package au.com.pks.challenge.controller;

import au.com.pks.challenge.domain.AverageBloodPressureForAgeRange;
import au.com.pks.challenge.domain.PatientsWithIncreasingGlucoseLevels;
import au.com.pks.challenge.domain.StandardStatisticsByGender;
import au.com.pks.challenge.service.PatientService;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.http.HttpStatus.OK;

@SpringBootTest
class PatientStatsControllerTest {

    @Mock
    private PatientService patientService;

    private PatientStatsController patientStatsController;

    @BeforeEach
    public void setup() {
        patientStatsController = new PatientStatsController(patientService);
    }

    @Test
    public void shouldReturnStandardStatisticsForAllPatients() {

        given(patientService.getStandardStatisticsForAllPatients()).willReturn(StandardStatisticsByGender.builder().build());
        ResponseEntity<StandardStatisticsByGender> response = patientStatsController.getStandardStatisticsForAllPatients();
        verify(patientService).getStandardStatisticsForAllPatients();
        MatcherAssert.assertThat(response.getStatusCode(), is(OK));

    }

    @Test
    public void shouldReturnAllPatientsWithIncreasingGlucoseLevel() {

        given(patientService.getAllPatientsWithIncreasingGlucoseLevel()).willReturn(PatientsWithIncreasingGlucoseLevels.builder().build());
        ResponseEntity<PatientsWithIncreasingGlucoseLevels> response = patientStatsController.getAllPatientsWithIncreasingGlucoseLevel();
        verify(patientService).getAllPatientsWithIncreasingGlucoseLevel();
        MatcherAssert.assertThat(response.getStatusCode(), is(OK));

    }

    @Test
    public void shouldReturnAverageBloodPressureForAgeRange() {

        given(patientService.getAverageBloodPressureForAgeRange(25, 45)).willReturn(AverageBloodPressureForAgeRange.builder().build());
        ResponseEntity<AverageBloodPressureForAgeRange> response = patientStatsController.getAverageBloodPressureForAgeRange(25, 45);
        verify(patientService).getAverageBloodPressureForAgeRange(25, 45);
        MatcherAssert.assertThat(response.getStatusCode(), is(OK));

    }

    @Test
    public void shouldThrowExceptionWhenReadingBadFile() {

        given(patientService.getAverageBloodPressureForAgeRange(25, 45)).willReturn(AverageBloodPressureForAgeRange.builder().build());
        ResponseEntity<AverageBloodPressureForAgeRange> response = patientStatsController.getAverageBloodPressureForAgeRange(25, 45);
        verify(patientService).getAverageBloodPressureForAgeRange(25, 45);
        MatcherAssert.assertThat(response.getStatusCode(), is(OK));

    }

}