package au.com.pks.challenge.controller;

import au.com.pks.challenge.domain.AverageBloodPressureForAgeRange;
import au.com.pks.challenge.domain.PatientsWithIncreasingGlucoseLevels;
import au.com.pks.challenge.domain.StandardStatisticsByGender;
import au.com.pks.challenge.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/patient/stats")
public class PatientStatsController {

    private final PatientService patientService;

    @Autowired
    public PatientStatsController(final PatientService patientService) {
        this.patientService = patientService;
    }

    @RequestMapping(value = "/standard")
    public ResponseEntity<StandardStatisticsByGender> getStandardStatisticsForAllPatients() {
        return new ResponseEntity<>(patientService.getStandardStatisticsForAllPatients(), OK);
    }

    @RequestMapping(value = "/glucose")
    public ResponseEntity<PatientsWithIncreasingGlucoseLevels> getAllPatientsWithIncreasingGlucoseLevel() {
        return new ResponseEntity<>(patientService.getAllPatientsWithIncreasingGlucoseLevel(), OK);
    }

    @RequestMapping(value = "/blood-pressure")
    public ResponseEntity<AverageBloodPressureForAgeRange> getAverageBloodPressureForAgeRange(@RequestParam final int startAge, @RequestParam final int endAge) {
        return new ResponseEntity<>(patientService.getAverageBloodPressureForAgeRange(startAge, endAge), OK);
    }

}
