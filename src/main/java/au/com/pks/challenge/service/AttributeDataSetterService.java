package au.com.pks.challenge.service;

import au.com.pks.challenge.domain.*;
import org.springframework.stereotype.Component;

import static au.com.pks.challenge.domain.PatientAttribute.*;

@Component
public class AttributeDataSetterService {

    public void setDataAttribute(final LabPatientDataRow getLabPatientDataRow, Patient patient) {

        switch (getLabPatientDataRow.getAttributeName()) {
            case AGE:
                patient.setAge(Integer.parseInt(getLabPatientDataRow.getAttributeValue()));
                break;
            case GENDER:
                patient.setGender(getLabPatientDataRow.getAttributeValue());
                break;
            case BLOOD_PRESSURE:
                PatientAttribute bloodPressureAttribute =
                        new BloodPressureAttribute(Integer.parseInt(getLabPatientDataRow.getAttributeValue()),
                                getLabPatientDataRow.getDate());
                patient.addAttribute(bloodPressureAttribute);
                break;
            case GLUCOSE:
                PatientAttribute glucoseAttribute =
                        new GlucoseAttribute(Float.parseFloat(getLabPatientDataRow.getAttributeValue()),
                                getLabPatientDataRow.getDate());
                patient.addAttribute(glucoseAttribute);
                break;
            case DIABETES:
                PatientAttribute diabetes =
                        new DiabetesAttribute(Boolean.parseBoolean(getLabPatientDataRow.getAttributeValue()),
                                getLabPatientDataRow.getDate());
                patient.addAttribute(diabetes);
                break;
            case WCC:
                PatientAttribute wcc =
                        new WccAttribute(Integer.parseInt(getLabPatientDataRow.getAttributeValue()),
                                getLabPatientDataRow.getDate());
                patient.addAttribute(wcc);
                break;
            default:
                throw new IllegalArgumentException("Attribute to build not recognised : " + getLabPatientDataRow.getAttributeName());
        }

    }

}
