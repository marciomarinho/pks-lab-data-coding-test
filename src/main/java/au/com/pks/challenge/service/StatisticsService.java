package au.com.pks.challenge.service;

import au.com.pks.challenge.domain.AverageBloodPressureForAgeRange;
import au.com.pks.challenge.domain.Patient;
import au.com.pks.challenge.domain.PatientsWithIncreasingGlucoseLevels;
import au.com.pks.challenge.domain.StandardStatisticsByGender;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

@Service
public class StatisticsService {

    public StandardStatisticsByGender getStandardStatisticsForAllPatients(List<Patient> patientsList) {

        Map<String, Long> countByGender = countByGender(patientsList);

        Map<String, Double> averageAgeByGender = averageAgeByGender(patientsList);

        return StandardStatisticsByGender.builder()
                .countByGender(countByGender)
                .averageAgeByGender(averageAgeByGender)
                .build();

    }

    //TODO: To be defined by the product manager in future iterations.

    /**
     * @param patientsList
     * @return A dummy object for patients with increasing glucose levels.
     */
    public PatientsWithIncreasingGlucoseLevels getAllPatientsWithIncreasingGlucoseLevel(List<Patient> patientsList) {
        patientsList.size();
        return PatientsWithIncreasingGlucoseLevels.builder()
                .patients(Arrays.asList(-1, -2, -3))
                .build();
    }

    //TODO: To be defined by the product manager in future iterations.

    /**
     * @param patientsList
     * @return A dummy object for average blood pressure on date rage.
     */
    public AverageBloodPressureForAgeRange getAverageBloodPressureForAgeRange(List<Patient> patientsList,
                                                                              final int startAge, final int endAge) {
        patientsList.size();
        return AverageBloodPressureForAgeRange.builder()
                .startAge(startAge)
                .endAge(endAge)
                .averageBloodPressure(75.8f)
                .build();
    }


    private Map<String, Double> averageAgeByGender(List<Patient> patientsList) {
        return patientsList
                .stream()
                .collect(groupingBy(Patient::getGender, averagingInt(Patient::getAge)));
    }

    private Map<String, Long> countByGender(List<Patient> patientsList) {
        return patientsList
                .stream()
                .collect(groupingBy(Patient::getGender, counting()));
    }

}
