package au.com.pks.challenge.service;

import au.com.pks.challenge.domain.LabDataType;
import au.com.pks.challenge.domain.LabPatientDataRow;
import au.com.pks.challenge.domain.Patient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Component
@Slf4j
public class LogFileReaderService {

    private static final String CSV_COLUMN_SEPARATOR = ",";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final AttributeDataSetterService attributeDataSetterService;

    @Autowired
    public LogFileReaderService(final AttributeDataSetterService attributeDataSetterService) {
        this.attributeDataSetterService = attributeDataSetterService;
    }

    public List<Patient> readLaboratoryData(final LabDataType labDataType) {

        Map<Integer, Patient> patientsMap = new HashMap<>();

        try (InputStream in = getClass().getClassLoader().getResourceAsStream(labDataType.getLogFileName())) {

            InputStreamReader inputStreamReader = new InputStreamReader(in);
            BufferedReader buf = new BufferedReader(inputStreamReader);
            String line;
            boolean isHeader = true;

            while ((line = buf.readLine()) != null) {
                if (isHeader) {
                    isHeader = false;
                    continue;
                }

                LabPatientDataRow getLabPatientDataRow = getLabPatientDataRow(line);

                Patient patient = getPatientById(patientsMap, getLabPatientDataRow);

                attributeDataSetterService.setDataAttribute(getLabPatientDataRow, patient);

                patientsMap.put(getLabPatientDataRow.getPatientId(), patient);

            }
        } catch (Exception e) {
            log.error("Something went wrong when trying to close the InputStream : ", e);
            return Collections.emptyList();
        }

        return patientsMap.entrySet()
                .stream()
                .map(e -> e.getValue())
                .collect(Collectors.toList());

    }

    private LabPatientDataRow getLabPatientDataRow(final String line) {

        String[] lineValues = line.split(CSV_COLUMN_SEPARATOR);

        return LabPatientDataRow.builder()
                .patientId(Integer.parseInt(lineValues[1].trim()))
                .date(LocalDate.parse(lineValues[0], formatter))
                .attributeName(lineValues[2].trim())
                .attributeValue(lineValues[3].trim())
                .build();

    }

    private Patient getPatientById(final Map<Integer, Patient> patientsMap, final LabPatientDataRow labPatientDataRow) {

        Patient patient = patientsMap.get(labPatientDataRow.getPatientId());

        if (isNull(patient)) {
            patient = Patient.builder()
                    .id(labPatientDataRow.getPatientId())
                    .date(labPatientDataRow.getDate())
                    .examAttribute(new ArrayList<>())
                    .build();
        }

        return patient;

    }

}
