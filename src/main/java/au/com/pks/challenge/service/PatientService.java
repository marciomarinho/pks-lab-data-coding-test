package au.com.pks.challenge.service;

import au.com.pks.challenge.domain.AverageBloodPressureForAgeRange;
import au.com.pks.challenge.domain.LabDataType;
import au.com.pks.challenge.domain.PatientsWithIncreasingGlucoseLevels;
import au.com.pks.challenge.domain.StandardStatisticsByGender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PatientService {

    private final LogFileReaderService logFileReaderService;
    private final StatisticsService statisticsService;

    @Autowired
    public PatientService(final LogFileReaderService logFileReaderService,
                          final StatisticsService statisticsService) {

        this.logFileReaderService = logFileReaderService;
        this.statisticsService = statisticsService;
    }

    public StandardStatisticsByGender getStandardStatisticsForAllPatients() {
        return statisticsService.getStandardStatisticsForAllPatients(logFileReaderService.readLaboratoryData(LabDataType.SAMPLE));
    }

    public PatientsWithIncreasingGlucoseLevels getAllPatientsWithIncreasingGlucoseLevel() {
        return statisticsService.getAllPatientsWithIncreasingGlucoseLevel(logFileReaderService.readLaboratoryData(LabDataType.SAMPLE));
    }

    public AverageBloodPressureForAgeRange getAverageBloodPressureForAgeRange(final int startAge, final int endAge) {
        return statisticsService.getAverageBloodPressureForAgeRange(logFileReaderService.readLaboratoryData(LabDataType.SAMPLE), startAge, endAge);
    }
}
