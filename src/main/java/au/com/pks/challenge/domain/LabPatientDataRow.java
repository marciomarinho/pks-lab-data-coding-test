package au.com.pks.challenge.domain;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class LabPatientDataRow {

    private int patientId;
    private LocalDate date;
    private String attributeName;
    private String attributeValue;

}
