package au.com.pks.challenge.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@EqualsAndHashCode(callSuper=false)
public class DiabetesAttribute extends PatientAttribute {

    private boolean value;

    public DiabetesAttribute(final boolean value, final LocalDate date) {
        super(DIABETES, date);
        this.value = value;
    }

}
