package au.com.pks.challenge.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@EqualsAndHashCode(callSuper=false)
public class GenderAttribute extends PatientAttribute {

    private String value;

    public GenderAttribute(final String value, final LocalDate date) {
        super(GENDER, date);
        this.value = value;
    }

}
