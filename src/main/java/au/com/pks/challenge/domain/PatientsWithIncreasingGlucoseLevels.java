package au.com.pks.challenge.domain;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class PatientsWithIncreasingGlucoseLevels {

    private List<Integer> patients;

}
