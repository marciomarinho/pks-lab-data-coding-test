package au.com.pks.challenge.domain;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class PatientAttribute {

    public static final String AGE = "Age";
    public static final String BLOOD_PRESSURE = "Blood pressure";
    public static final String GENDER = "Gender";
    public static final String GLUCOSE = "Glucose";
    public static final String DIABETES = "Diabetes";
    public static final String WCC = "WCC";

    private String attributeName;
    private LocalDate date;

    protected PatientAttribute(final String attributeName, final LocalDate date) {
        this.attributeName = attributeName;
        this.date = date;
    }

}
