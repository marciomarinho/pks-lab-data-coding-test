package au.com.pks.challenge.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@EqualsAndHashCode(callSuper=false)
public class WccAttribute extends PatientAttribute {

    private int value;

    public WccAttribute(final int value, final LocalDate date) {
        super(WCC, date);
        this.value = value;
    }

}
