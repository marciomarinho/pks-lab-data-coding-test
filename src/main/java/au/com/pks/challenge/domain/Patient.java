package au.com.pks.challenge.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class Patient {

    private long id;
    private int age;
    private String gender;
    private LocalDate date;

    private List<PatientAttribute> examAttribute;

    public void addAttribute(PatientAttribute newAttribute) {
        this.examAttribute.add(newAttribute);
    }
}
