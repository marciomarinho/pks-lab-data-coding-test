package au.com.pks.challenge.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AverageBloodPressureForAgeRange {

    private int startAge;
    private int endAge;
    private float averageBloodPressure;

}
