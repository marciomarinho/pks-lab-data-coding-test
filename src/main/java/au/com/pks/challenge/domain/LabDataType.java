package au.com.pks.challenge.domain;

public enum LabDataType {

    SAMPLE("messages.csv"),
    BAD_FORMATTED_FILE("bad_messages.csv");

    private final String logFileName;

    LabDataType(final String logFileName) {
        this.logFileName = logFileName;
    }

    public String getLogFileName() {
        return logFileName;
    }

}
