package au.com.pks.challenge.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@EqualsAndHashCode(callSuper=false)
public class BloodPressureAttribute extends PatientAttribute {

    private int value;

    public BloodPressureAttribute(final int value, final LocalDate date) {
        super(BLOOD_PRESSURE, date);
        this.value = value;
    }

}
