package au.com.pks.challenge.domain;

import lombok.Builder;
import lombok.Value;

import java.util.Map;

@Value
@Builder
public class StandardStatisticsByGender {

    private Map<String, Long> countByGender;
    private Map<String, Double> averageAgeByGender;

}
