package au.com.pks.challenge.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@EqualsAndHashCode(callSuper=false)
public class GlucoseAttribute extends PatientAttribute {

    private float value;

    public GlucoseAttribute(final float value, final LocalDate date) {
        super(GLUCOSE, date);
        this.value = value;
    }

}
